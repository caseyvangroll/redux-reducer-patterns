const store = require('./store');
const actionTypes = require('./model');

const {CLASS_LIST} = actionTypes;

console.log(store.getState());
store.dispatch(CLASS_LIST.SET, {id: 1, name: 'bob'});
console.log(store.getState());
store.dispatch(CLASS_LIST.SET, {id: 2, name: 'bill'});
console.log(store.getState());
store.dispatch(CLASS_LIST.UNSET, {id: 2});
console.log(store.getState());
