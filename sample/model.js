/* eslint-disable new-cap */
const {ARRAY, VALUE} = require('../src/modelDataTypes');
const configureModel = require('../src/configureModel');
const sortFunc = require('./sortFunc');

const model = {
  IS_LOGGED_IN: VALUE(),
  STATUS: VALUE(),
  OPEN_CLASS: {
    ID: VALUE(),
    NAME: VALUE(),
    IS_LOADING: VALUE(),
    IS_MODAL_VISIBLE: VALUE(),
    IS_MODAL_EDIT_MODE: VALUE(),
  },
  OPEN_RULE: {
    ID: VALUE(),
    SHOULD_GROUP: VALUE(),
    STUDENT_LIST: ARRAY(),
    IS_MODAL_VISIBLE: VALUE(),
    IS_MODAL_EDIT_MODE: VALUE(),
  },
  OPEN_ASSIGNMENT: {
    ID: VALUE(),
    NAME: VALUE(),
    IS_MODAL_VISIBLE: VALUE(),
    IS_SAVED: VALUE(),
    TARGETING_MODE: VALUE('wow'),
    TARGET_GROUP_SIZE: VALUE(),
    TARGET_GROUP_COUNT: VALUE(),
    GROUPS: ARRAY(),
  },
  CLASS_LIST: ARRAY([], sortFunc),
  IS_CLASS_LIST_VISIBLE: VALUE(),
  IS_CLASS_LIST_LOADING: VALUE(),
  STUDENT_LIST: ARRAY(),
  RULE_LIST: ARRAY(),
  ASSIGNMENT_LIST: ARRAY(),
};

configureModel(model);

module.exports = model;
