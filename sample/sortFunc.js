module.exports = (a, b) => {
  const aName = a.name.toLowerCase();
  const bName = b.name.toLowerCase();
  if (aName === bName) {
    return caseInsensitiveIdSort(a, b);
  }
  return aName < bName ? -1 : 1;
};
