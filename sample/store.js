const {createStore, applyMiddleware} = require('redux');
const createModelReducer = require('../src/createModelReducer');
const modelMiddleware = require('../src/modelMiddleware');
const actionTypes = require('./model');

const rootReducer = createModelReducer(actionTypes);

module.exports = createStore(
    rootReducer,
    applyMiddleware(
        modelMiddleware
    )
);
