const _set = require('lodash.set');
const _get = require('lodash.get');
const _isString = require('lodash.isstring');
const _isObject = require('lodash.isobject');
const _snakeCase = require('lodash.snakecase');

const configureModel = (actionTypes, path) => {
  const value = _get(actionTypes, path, actionTypes);
  if (_isString(value)) {
    const newValue = path
        .split('.')
        .map((piece) => _snakeCase(piece).toUpperCase())
        .join('.');
    _set(actionTypes, path, newValue);
  } else if (_isObject(value)) {
    Object.keys(value).forEach((key) => {
      if (!['_initialState', '_sortFunc'].includes(key)) {
        configureModel(actionTypes, `${path ? `${path}.` : ''}${key}`);
      }
    });
  }
};

module.exports = configureModel;
