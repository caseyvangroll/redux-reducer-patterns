const _get = require('lodash.get');
const _isString = require('lodash.isstring');
const _isObject = require('lodash.isobject');
const _camelCase = require('lodash.camelcase');
const arrayReducer = require('./reducers/arrayReducer');
const valueReducer = require('./reducers/valueReducer');

const isLeafReducer = (o) => _isString(Object.values(o)[0]);

const getLeafReducer = (value) => {
  if (value.CLEAR) {
    return arrayReducer;
  }
  return valueReducer;
};

const createModelReducer = (actionTypes, path = '') => {
  const node = _get(actionTypes, path, actionTypes);

  if (isLeafReducer(node)) {
    const {_initialState, _sortFunc, ...leafActionTypes} = _get(actionTypes, path);
    return getLeafReducer(node)(
      leafActionTypes,
      _initialState,
      _sortFunc
    );
  }

  return (state, action) => Object.keys(node).reduce((acc, key) => {
    const substate = _isObject(state) ? state[_camelCase(key)] : undefined;
    const childPath = `${path ? `${path}.` : ''}${key}`;
    const childReducer = createModelReducer(actionTypes, childPath)(substate, action);
    return Object.assign({}, acc, {[_camelCase(key)]: childReducer});
  }, {});
};

module.exports = createModelReducer;

