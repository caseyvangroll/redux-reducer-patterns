const modelMiddleware = require('./modelMiddleware');
const modelDataTypes = require('./modelDataTypes');
const configureModel = require('./configureModel');
const createModelReducer = require('./createModelReducer');

module.exports = {
  modelMiddleware,
  modelDataTypes,
  configureModel,
  createModelReducer,
};
