const VALUE = (_initialState) => ({
  SET: 'SET',
  UNSET: 'UNSET',
  _initialState,
});

const ARRAY = (_initialState, _sortFunc) => ({
  SET: 'SET',
  UNSET: 'UNSET',
  CLEAR: 'CLEAR',
  _initialState,
  _sortFunc,
});

module.exports = {
  VALUE,
  ARRAY,
};
