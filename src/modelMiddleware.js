const argumentsMiddleware = require('redux-arguments-middleware');

module.exports = argumentsMiddleware((args) => {
  switch (args.length) {
    case 1:
      return {entry: args[0]};
    default:
      return {};
  }
});
