module.exports = ({SET, UNSET, CLEAR}, initialState = [], sortFunc) => {
  if ([SET, UNSET, CLEAR].some((type) => type === undefined)) {
    throw new Error(
        'Must define all actionTypes (SET, UNSET, CLEAR) to use arrayReducer.',
        JSON.stringify({SET, UNSET, CLEAR})
    );
  }

  // Returns a reducer
  return (state = initialState, action) => {
    const {type, entry} = action;
    switch (type) {
      case SET: {
        let alreadyExisted;
        const nextState = state.map((element) => {
          if (element.id === entry.id) {
            alreadyExisted = true;
            return entry;
          }
          return element;
        });
        if (!alreadyExisted) {
          nextState.push(entry);
        }
        return sortFunc ? nextState.sort(sortFunc) : nextState;
      }
      case UNSET:
        return state.filter((element) => element.id !== entry.id);
      case CLEAR:
        return [];
      default:
        return state;
    }
  };
};
