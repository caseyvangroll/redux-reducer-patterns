module.exports = ({SET, UNSET}, initialState) => {
  if (SET === undefined) {
    throw new Error(
        'Must define SET action to use valueReducer.',
        JSON.stringify({SET, UNSET})
    );
  }

  // Returns a reducer
  return (state = initialState, action) => {
    switch (action.type) {
      case SET: {
        return action.entry;
      }
      case UNSET:
        return undefined;
      default:
        return state;
    }
  };
};
